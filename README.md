# README #

Hello, I'd appreciate it, if you found this page, because some bot crawled it at one point, I'd appreciate if you
don't copy/paste solutions to project euler. Please take the time to read the following disclaimer. 

# Project Euler Disclaimer #
DISCLAIMER

The popularity of Project Euler has increased tremendously over the years since its inception through the concerted effort of numerous people.

Members have generally taken pride in showing their achievements. The obvious place to do so is the forum that is set up for each problem. However the increasing popularity has also been attracting people with a different agenda. With so many internet sites publishing answers for the PE problems instead of using the forum on this site, some people now seem to take pride in gathering and submitting those answers for whatever reason.

It is humanly impossible for Project Euler, or indeed any member, to reliably differentiate those people from members that have solved the problems under their own steam. The ranking lists on Project Euler should be interpreted for what they are: an indication of the number of correct answers submitted by the member in question and validated by our answer checker. Individual members themselves know best what they have achieved on their own. Laying too much stress on competition might spoil the fun of problem solving which is one of the purposes of Project Euler. Also, any claim from a member about his own achievement cannot be supported as trustworthy by Project Euler. If critical, other means from third parties would be required to verify such claims.

Providing quality problems for entertainment and educational purposes will continue to be the main goal of Project Euler, regardless of the intentions of a minority of the members.
