package com.ewyx.euler._0x

import com.ewyx.euler.core.EulerProblem

/**
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 *  Find the sum of all the multiples of 3 or 5 below 1000.
 */
class Euler1 extends EulerProblem[Int] {
  override def solve(): Int = {
    (1 until 1000)
      .filter(el => el % 3 == 0 || el % 5 == 0).sum
  }
}
