package com.ewyx.euler._0x
import com.ewyx.euler.core.Utils

/**
 *
 * The prime factors of 13195 are 5, 7, 13 and 29.
 *
 * What is the largest prime factor of the number 600851475143 ?
 *
 * v.1 time: 9.028117ms
 * v.2 time: 7.759941ms
 *
 * possible improvements, lazy loads etc.
 */

object Euler3 {

  def isPrime(n: Int): Boolean = {
    val divisors = Stream.from(2) //.filter(x => x % 2 == 0 && x > 0)
    divisors.map(n % _).take(n / 2 - 1).filter(_ == 0).length == 0
  };
  //This should be done much more nicely.
  val sPrimes = (2 to 7000).toList.filter(isPrime(_))

  def getFirstPrimeDivisorPos(n: Long, primes: List[Int]) = {
    primes.map(n % _).takeWhile(_ != 0).length
  }

  def primeDivisors(n: Long): List[Int] = {
    primeDivisors(n, List(), sPrimes)
  }

  def primeDivisors(n: Long, acc: List[Int], primes: List[Int]): List[Int] = {
    n match {
      case 1L => acc
      case _ => {
        val pPos = getFirstPrimeDivisorPos(n, primes)
        val pDiv = primes.drop(pPos).head
        pDiv :: primeDivisors(n / pDiv, acc, primes.drop(pPos - 1))
      }
    }
  }
  def solve() = {
    val startingNumber: Long = 600851475143L;
    val factors = List()
    Utils.time {
      primeDivisors(startingNumber)
    }
  }
}