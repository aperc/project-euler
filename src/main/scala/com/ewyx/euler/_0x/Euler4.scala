package com.ewyx.euler._0x
import com.ewyx.euler.core.Utils

/**
 *
 * A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 * 99.
 *
 * Find the largest palindrome made from the product of two 3-digit numbers.
 *
 * notes:
 *  - find a better way to get reversed numbers, more functional way
 *  - remove duplicate cross products.
 *
 * ver1: 7498.168911ms
 */
object Euler4 {
  val maxNum = 998001
  def rev(n: Int): Int = {
    val i = n % 10
    val j = n % 100 / 10;
    val k = n % 1000 / 100
    i * 100 + j * 10 + k
  }
  val palindromes = for (a <- List.range(100, 999)) yield a * 1000 + rev(a)
  val prods = for (
    a <- (100 until 1000);
    b <- (100 until 1000)
  ) yield a * b

  def solve() = {
    prods.filter(palindromes.contains(_)).sortWith(_ < _)
  }
}