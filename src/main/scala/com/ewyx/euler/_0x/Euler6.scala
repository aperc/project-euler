package com.ewyx.euler._0x
/*
 * 
 * 
 * 
 *
 *	The sum of the squares of the first ten natural numbers is,
 *	1^2 + 2^2 + ... + 10^2 = 385
 *	
 *	The square of the sum of the first ten natural numbers is,
 *	(1 + 2 + ... + 10)2 = 552 = 3025
 *	
 *	Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
 *	
 *	Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 * 
 * 	Note: Meh, guess bruteforcing works.
 * 
 */
object Euler6 {

  def solve(): Long = {
    val r = 1L to 100L
    val x = r.sum
    x * x - r.map(x => x * x).sum
  }
}