package com.ewyx.euler._0x

import com.ewyx.euler.core.{ Utils, EulerProblem }

class Euler7 extends EulerProblem[Int] {
  override def solve(): Int = Utils.primes().drop(10000).head
}
