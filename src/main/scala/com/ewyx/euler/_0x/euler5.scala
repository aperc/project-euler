package com.ewyx.euler._0x
import com.ewyx.euler.core.Utils

object Euler5 {
  /**
   *
   * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
   *
   * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
   *
   */

  def solve() = {
    //short explanation
    //we find the prime factors of each number.
    //we then find the intersections of all sets, we do this with a reduce operation.
    //what we do, is add the difference between the reduced set, and the next element in the list,
    //which in the end leads us to the smallest intersection.
    (2 to 20)
      .map(Utils.factorize)
      .reduce((x, y) => x ::: y.diff(x))
      .product
  }
}