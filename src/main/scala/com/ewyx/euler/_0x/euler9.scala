package com.ewyx.euler._0x

object Euler9 {

  /**
   *
   * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
   * a2 + b2 = c2
   *
   * For example, 32 + 42 = 9 + 16 = 25 = 52.
   *
   * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
   * Find the product abc.
   *
   *
   * Not the smartest solution.
   * Basically we're stabbing in the dark and trying to find the solution to the equation Sqrt(a^2 + b^2) + a + b = 1000;
   * We find all the candidates, and then we see which one fits. Since we know a,b,c are natural numbers, we can make sure that
   * (a^2 + b^2) is also a square. Honestly, I think blind guessing is more effective than this, but at least I got to use some
   * scala concepts. *ugh*
   */

  def solve() = {
    val squares = Stream.from(1).takeWhile(_ < 1000).map(x => x * x).toList

    val candidatePairs = for {
      a <- squares
      b <- squares
      if (squares.contains(a + b))
    } yield List(a, b)

    val resPairs = candidatePairs
      .filter(x => x.head > x.last && squares.contains(x.head + x.last))
      .filter(x => math.sqrt(x.head + x.last) + math.sqrt(x.head) + math.sqrt(x.last) == 1000)

    val aAndB = resPairs.flatten.map(math.sqrt(_).toInt)
    val c = aAndB.foldLeft(1000)((x, y) => x - y)
    aAndB.foldLeft(c)((x, y) => x * y)
  }
}