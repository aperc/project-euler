package com.ewyx.euler._1x

import com.ewyx.euler.core.{ Utils, EulerProblem }

class Euler10 extends EulerProblem[Long] {
  def solve(): Long = {
    Utils.sieveOfEratosthenes(2000000).map(_.toLong).sum
  }
}
