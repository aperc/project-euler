package com.ewyx.euler._1x
import com.ewyx.euler.core.Utils

object Euler12 {

  //n(n+1)/2
  lazy val triangle: Stream[Int] = Stream.from(1).map(x => (x * (x + 1)) / 2)

  // Implemented the following formula
  // http://en.wikipedia.org/wiki/Divisor_function#Properties
  def nDivisors(n: Int) = Utils.factorize(n)
    .groupBy(x => x)
    .map(_._2.length + 1)
    .foldLeft(1)(_ * _)

  def solve() = {
    triangle.dropWhile(nDivisors(_) < 500).head
  }
}