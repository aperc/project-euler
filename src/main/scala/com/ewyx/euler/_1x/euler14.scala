package com.ewyx.euler._1x

import scala.collection.mutable.HashMap
import scala.annotation.tailrec

object Euler14 {
  /**
   *
   * The following iterative sequence is defined for the set of positive Integers:
   *
   * n -> n/2 (n is even)
   * n -> 3n + 1 (n is odd)
   *
   * Using the rule above and starting with 13, we generate the following sequence:
   * 13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
   *
   * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
   *
   * Which starting number, under one million, produces the longest chain?
   *
   * NOTE: Once the chain starts the terms are allowed to go above one million.
   *
   */

  def even(x: Long): Long = x / 2
  def odd(x: Long): Long = 3 * x + 1
  val lengthMap: HashMap[Long, Long] = HashMap()

  @tailrec
  def findLength(x: Long, length: Long): Long = {
    if (lengthMap.contains(x)) {
      lengthMap.get(x).get + length - 1
    } else {
      val newX = x match {
        case x if x % 2 == 0 => even(x)
        case x if x % 2 != 0 => odd(x)
      }
      if (newX == 1) {
        length + 1
      } else {
        findLength(newX, length + 1)
      }
    }
  }

  def solve() = {
    (1 to 1000000 by 2).foreach(x => {
      lengthMap.put(x, findLength(x, 1))
    })
    lengthMap.maxBy(x => x._2)
  }

}