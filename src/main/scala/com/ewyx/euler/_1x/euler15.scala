package com.ewyx.euler._1x

import scala.collection.mutable.HashMap

//apparently there's also a combinatorial solution r = n!/r!(n-r)!
//But hey, let's practice some functional programming instead.r

object Euler15 {

  type Point = (Int, Int)
  val bufferMap: HashMap[Point, Long] = HashMap()

  def findPaths(x: Point, acc: Long): Long = {
    if (bufferMap.contains(x)) {
      bufferMap.getOrElse(x, -1)
    } else {
      x match {
        case (20, 20) => 1
        case (_, 20) => acc + findPaths((x._1 + 1, x._2), acc)
        case (20, _) => acc + findPaths((x._1, x._2 + 1), acc)
        case (_, _) => acc + findPaths((x._1 + 1, x._2), acc) + findPaths((x._1, x._2 + 1), acc)
      }
    }
  }

  def solve(): Long = {
    for {
      x <- 20 until 0 by -1
      y <- 20 until 0 by -1
    } bufferMap.put((x, y), findPaths((x, y), 0))
    findPaths((0, 0), 0)
  }
}