package com.ewyx.euler._1x

object Euler16 {
  val n = List(2)

  def f(l: List[Int], n: Int): List[Int] = {
    l match {
      case x :: xs => List((x + n * 2) / 10, (x + n * 2) % 10) ::: xs
    }
  }
  def mby2(n: List[Int]): List[Int] = {
    val e: List[Int] = List()
    n.foldLeft(List(0))(f).reverse
  }
  def powerOfTwo(rep: Int, l: List[Int]): List[Int] = {
    rep match {
      case 1 => l
      case _ => powerOfTwo(rep - 1, mby2(l))
    }
  }
  def solve() = powerOfTwo(1000, n) sum
}