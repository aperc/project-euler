package com.ewyx.euler._1x
/**
 *
 *
 * If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 *
 * If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
 *
 * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters.
 * The use of "and" when writing out numbers is in compliance with British usage.
 *
 *
 */
object Euler17 {
  val numberCounts = List(0, 3, 3, 5, 4, 4, 3, 5, 5, 4, 3, 6, 6, 8, 8, 7, 7, 9, 8, 8)
  val timesTenCount = List(0, 3, 6, 6, 5, 5, 5, 7, 6, 6) //twenty, thirty, forty, fifty, sixty, seventy, eighty, ninety) 
  val hundred = 7
  val thousand = 8

  def countLetters(n: Int): Int = {
    val restCount = n % 100 match {
      case rest if rest == 0 => 0
      case rest if rest < 20 => numberCounts(rest)
      case rest if rest >= 20 => timesTenCount(rest / 10) + numberCounts(rest % 10)
    }
    val restWithAnd = if (n / 100 > 0 && n % 100 > 0) restCount + 3 else restCount
    if (n / 100 != 0) {
      numberCounts(n / 100) + hundred + restWithAnd
    } else {
      restWithAnd
    }
  }

  def solve() = {
    (1 to 999 map (countLetters(_)) reduce (_ + _)) + 11
  }
}