package com.ewyx.euler._1x
import java.util.Calendar

object Euler19 {
  //christ this is a cheap solution
  def solve(): Int = {
    val cal = Calendar.getInstance()
    cal.set(1900, 10, 1)

    def isSunday = cal.get(Calendar.DAY_OF_WEEK).equals(Calendar.SUNDAY)

    (1 to 12 * 100).count(x => {
      cal.add(Calendar.MONTH, 1)
      isSunday
    })

  }
}