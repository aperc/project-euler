package com.ewyx.euler._2x

import scala.annotation.tailrec

object Euler26 {
  @tailrec
  def f(n: Int, r: List[(Int, Int)]): List[(Int, Int)] = {
    val newVal = ((r.head._1 * 10) % n, (r.head._1 * 10) / n)
    def isRecurring(): Boolean = r.map(_._1).contains(newVal._1)
    if (!isRecurring())
      f(n, newVal :: r)
    else
      r
  }
  val r = for {
    x <- 1 to 1000
  } yield f(x, List((1, x)))

  //add + 1, because of index.
  def solve() = r.map(_.length).zipWithIndex.sortWith(_._1 > _._1).head._2 + 1

}