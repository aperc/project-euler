package euler

import scala.annotation.tailrec
import com.ewyx.euler.core.Utils

object Euler20 {
  def f(mul: Int)(l: List[Int], n: Int): List[Int] = {
    l match {
      case x :: xs => List((x + n * mul) / 10, (x + n * mul) % 10) ::: xs
    }
  }

  def m(n: List[Int], mul: Int): List[Int] = {
    val e: List[Int] = List()
    val res = n.foldLeft(List(0))(f(mul))
    (Utils.digits(res.head) ::: res.tail) reverse
  }

  @tailrec
  def longFact(x: Int, l: List[Int] = List(1), n: Int = 0): List[Int] = {
    n match {
      case n: Int if (n == x) => l
      case _ => longFact(x, m(l, n + 1), n + 1)
    }
  }

  def solve() = longFact(100) sum
}