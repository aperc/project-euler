package com.ewyx.euler._2x
import com.ewyx.euler.core.Utils

object Euler21 {
  //buffering some calculations reduces from 650ms to 500ms
  val store: Array[Option[Int]] = Array.fill(30000)(None)

  def sumOfProperDivisors(number: Int): Int = {
    val bufferedVal = store(number - 1)

    bufferedVal match {
      case None => {
        def factors = Utils.factorize(number).groupBy(x => x)
        factors.map(x => (x._1, x._2.length)).toList
        val sum = Utils.divisorSum(factors.map(x => (x._1, x._2.length)).toList) - number
        store(number - 1) = Some(sum)
        sum
      }
      case Some(x) => x
    }
  }

  def solve() = {
    val a = for {
      x <- 2 to 10000
      if (x == sumOfProperDivisors(sumOfProperDivisors(x)) && sumOfProperDivisors(x) != x)
    } yield x
    a.reduce(_ + _)
  }
}