package com.ewyx.euler._2x

import com.ewyx.euler._

object Euler22 {

  def solve() = {
    val source = loadFile("names.txt").getLines.mkString
    val names = source.toString.split(",")
      .map(x => x.substring(1, x.length - 1))
      .toList.sorted.zipWithIndex
    names.map(x => x._1.map(_.toInt - 64).sum * (x._2 + 1)).sum
  }
}