package com.ewyx.euler._2x
import com.ewyx.euler.core.Utils

object Euler23 {

  //bruteforce solution
  val store: Array[Int] = Array.fill(30000)(-1)

  def sumOfProperDivisors(number: Int): Int = {
    def factors = Utils.factorize(number).groupBy(x => x)
    factors.map(x => (x._1, x._2.length)).toList
    Utils.divisorSum(factors.map(x => (x._1, x._2.length)).toList) - number
  }

  def solve() = {
    val abundant = for {
      x <- 2 to 28123
      if sumOfProperDivisors(x) > x
    } yield x
    val sums = for {
      x <- abundant
      y <- abundant.takeWhile(_ + x <= 28123)
      //if(x+y <= 28123)
    } yield x + y
    (1 to 28123).toList.diff(sums.distinct).sum
  }
}