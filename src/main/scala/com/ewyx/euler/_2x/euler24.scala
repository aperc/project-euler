package com.ewyx.euler._2x
import com.ewyx.euler.core.Utils

object Euler24 {

  def factorial(n: Int) = {
    if (n == 0) 1 else 1 to n reduce (_ * _)
  }

  val numbers = (0 to 9) toList
  val factors = (0 to 9) map (factorial(_)) reverse
  val possibleFactors = 0 to 9 toList

  def highestAvailableFactor(startingValue: Int, multiplicant: Int, taken: List[Int]): (Int, Int, Int) = {
    val factorList = for {
      factor <- (0 to numbers.length - taken.length - 1)
      if (factor * multiplicant + startingValue < 1000000)
    } yield factor

    (factorList.length - 1,
      possibleFactors.filterNot(taken.contains(_))(factorList.length - 1),
      multiplicant);
  }

  def findFactors: List[(Int, Int, Int)] = {
    factors.foldLeft(List.empty[(Int, Int, Int)])((res: List[(Int, Int, Int)], f: Int) => {
      val startingValue = res match {
        case Nil => 0
        case _ => res map (x => x._1 * x._3) reduceLeft (_ + _)
      }
      val takenVals = res map (_._2)
      highestAvailableFactor(startingValue, f, takenVals) :: res
    })
  }

  def solve() = {
    findFactors.reverse.map(_._2)
  }
}