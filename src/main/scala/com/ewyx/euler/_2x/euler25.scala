package com.ewyx.euler._2x
import scala.math._

object Euler25 {
  //use the binet formula
  //http://www.had2know.com/academics/number-digits-length-fibonacci-number.html
  def termLength(n: Int): Int = {
    val phi = 1.6180339887498948482045868
    floor(n * log10(phi) - log10(5) / 2 + 1).toInt
  }

  def solve() = {
    println(termLength(4782))
    Stream.from(4000).map(x => termLength(x)).takeWhile(_ < 1000).length + 4000
  }
}