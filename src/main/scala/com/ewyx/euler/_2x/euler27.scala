package com.ewyx.euler._2x
import scala.math.pow
import com.ewyx.euler.core.Utils

object Euler27 {
  val candidatesB = Utils.sieveOfEratosthenes(1000)
  def consecutivePrimes(a: Int, b: Int): Int = {
    Stream.from(0).map(x => pow(x, 2) + a * x + b).takeWhile(x => Utils.isPrime(x.toInt)).length
  }
  def getResult = {
    val pairs = for {
      a <- -1000 until 1000
      b <- candidatesB
    } yield (a, b)
    pairs map (x => (consecutivePrimes(x._1, x._2), x))
  }

  def solve() = {
    val results = getResult sortWith (_._1 > _._1)
    val res = (results head)._2
    res._1 * res._2
  }
}