package com.ewyx.euler._2x

object Euler29 {
  //this should be done with factorization though. Doubles are bad, or something.
  def solve() = {
    val powers = for {
      a <- 2 to 100
      b <- 2 to 100
    } yield math.pow(a, b)
    powers.distinct.length
  }
}