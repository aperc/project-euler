package com.ewyx.euler._3x

object Euler38 {

  def convertToCandidate(r: List[Int]): String = r.map(_.toString).reduceLeft(_ + _).take(9)

  def isPandigital(v: List[Int]): Boolean = {
    val digit = convertToCandidate(v)
    digit.distinct.length == 9 && !digit.contains('0')
  }

  def solve() = {
    val candidates = for {
      i <- ((1 to 10000) map (x => (1 to 9) map (_ * x) toList))
      if (isPandigital(i))
    } yield convertToCandidate(i)
    candidates.max
  }
}