package com.ewyx.euler._3x

import scala.annotation.tailrec

object Euler30 {
  //faster than maping a string...
  @tailrec
  def digits(n: Int, d: List[Int] = List()): List[Int] = {
    n match {
      case n: Int if n / 10 == 0 => n :: d
      case _ => digits(n / 10, n % 10 :: d)
    }
  }

  def solve() = {
    val pow5 = 0 to 9 map (math.pow(_, 5).toInt)
    def dTo5(n: Int): Int = digits(n).map(pow5).sum
    val powers = for {
      a <- 2 to 354294
      if (a == dTo5(a))
    } yield a

    powers.sum
  }
}