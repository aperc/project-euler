package com.ewyx.euler._3x

object Euler31 {
  val denominations = List(1, 2, 5, 10, 20, 50, 100, 200)

  def combinations(total: Int, max: Int): Int = {
    val options = denominations.filter(_ <= max)
    total match {
      case x: Int if (x > 0) => options.map(x => combinations(total - x, x)).sum
      case x: Int if (x == 0) => 1
      case _ => 0
    }
  }

  def solve() = {
    combinations(200, 200)
  }
}