package com.ewyx.euler._3x
import com.ewyx.euler.core.Utils

object Euler32 {

  def isPandigital(x: Int, y: Int, n: Int): Boolean = {
    val d = Utils.digits(x) ++ Utils.digits(y) ++ Utils.digits(n)
    d.length == 9 && d.distinct.length == 9 && !d.contains(0)
  }

  def solve() = {
    val pairs = for {
      i <- 1 to 100
      j <- 1 to 2000
      if (isPandigital(i, j, i * j) && i != j)
    } yield (i, j)
    pairs.map(x => x._1 * x._2).distinct.sum
  }
}
