package com.ewyx.euler._3x

object Euler33 {

  def toNumber(x: (Int, Int)) = x._1 * 10 + x._2

  def isTrivial(x: ((Int, Int), (Int, Int))): Boolean = x._1._1 * x._1._2 * x._2._1 * x._2._2 == 0

  def isCurious(x: ((Int, Int), (Int, Int))): Boolean = {
    toNumber(x._1).toFloat / toNumber(x._2).toFloat == x._1._1.toFloat / x._2._2.toFloat &&
      x._1._2 == x._2._1
  }
  def solve() = {
    val fractions = for {
      i <- 10 to 99
      j <- 10 to 99
      if (j > i)
    } yield ((i / 10, i % 10), (j / 10, j % 10))
    val fractionDigits = fractions.filterNot(isTrivial)
    val curiosDigits = fractionDigits.filter(isCurious).map(x => (toNumber(x._1), toNumber(x._2)))
    val result = curiosDigits reduceLeft ((x, y) => (x._1 * y._1, x._2 * y._2))
    result._2 / result._1
  }
}