package com.ewyx.euler._3x
import com.ewyx.euler.core.Utils

object Euler34 {
  val facList = (0 to 10).map(Utils.factorial)
  def factorialSum(n: Int) = {
    Utils.digits(n).map(facList(_)).sum
  }
  def solve() = {
    val sums = for {
      i <- 10 to 100000;
      if (factorialSum(i) == i)
    } yield i
    sums.sum
  }
}