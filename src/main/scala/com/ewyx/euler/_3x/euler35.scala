package com.ewyx.euler._3x
import com.ewyx.euler.core.Utils._

object Euler35 {

  val primes = sieveOfEratosthenes(1000000).zipWithIndex.toMap

  def cycles(n: Int) = {
    val cycles = math.log10(n).floor.toInt
    (0 to cycles) map (x => (n / math.pow(10, x)).toInt + n % math.pow(10, x).toInt * math.pow(10, cycles + 1 - x))
  }

  def isCircular(n: Int): Boolean = cycles(n).flatMap(x => primes.get(x.toInt)).length == math.log10(n).floor.toInt + 1

  def solve() = {
    var counter = 0
    for {
      n <- primes.keys
      if (isCircular(n))
    } counter = counter + 1
    counter
  }
}