package euler

import com.ewyx.euler.core.Utils

object Euler36 {
  def palindrome(n: Int): List[Int] = {
    //should be done better, toString and back toInt isn't fast.
    val d = Utils.digits(n)
    List(
      (d ::: d.reverse).map(_.toString).reduce(_ + _).toInt,
      (d ::: d.reverse.tail).map(_.toString).reduce(_ + _).toInt
    )
  }
  def isBinaryPalindrome(n: Int): Boolean = {
    val binary = n.toBinaryString
    binary == binary.reverse
  }

  def solve() = {
    val pals = for { //generate all possible palindromes in decimal
      i <- 1 until 1000
    } yield palindrome(i)
    pals.flatten.filter(isBinaryPalindrome).sum
  }
}