package com.ewyx.euler._3x
import com.ewyx.euler.core.Utils._
import com.ewyx.euler.core.Utils

object Euler37 {
  // the primary number can't contain any of these
  val forbidden = List(4, 6, 8, 0)
  def lTruncatable(n: List[Int], reverse: Boolean = false): Boolean = {
    n match {
      case x :: xs => if (isPrime(digitsToInt(n))) lTruncatable(xs) else false
      case _ => true
    }
  }

  def rTruncatable(n: List[Int]): Boolean = {
    n.reverse match {
      case x :: xs => if (isPrime(digitsToInt(n))) rTruncatable(xs.reverse) else false
      case _ => true
    }
  }

  def truncatable(n: Int): Boolean = {
    val digits = Utils.digits(n)
    val safe = digits == digits.diff(forbidden)
    safe && lTruncatable(digits) && rTruncatable(digits)
  }

  def solve() = {
    //Note, this could be done a lot more smarter.
    //We could generate our own candidates, and build
    //truncatable primes.
    val primes = for {
      i <- Utils.sieveOfEratosthenes(1000000)
      if truncatable(i)
    } yield i
    primes.filter(_ > 10).sum
  }
}