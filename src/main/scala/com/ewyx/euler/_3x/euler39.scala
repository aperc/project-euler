package com.ewyx.euler._3x
import com.ewyx.euler.core.Utils

object Euler39 {
  def solve() = {
    //Potential upgrade, find a good way to generate limits.
    val sides = for {
      i <- 1 to 100
      j <- 1 to 100
      k <- 1 to 100
      if (i > j && k * (i * i + j * j) < 1000 && Utils.coprimes(i, j))
    } yield List(
      k * (i * i - j * j),
      k * (2 * i * j),
      k * (i * i + j * j)
    )

    sides.filter(x => x.sum <= 1000) //filter p <= 1000
      .map(_.sortWith(_ > _)).distinct.map(_.sum) // get unique triplets and their sums
      .groupBy(x => x).mapValues(_.length) // group by sum, count sums
      .toList.sortBy(_._2).last // sort by count and take the last element
  }
}