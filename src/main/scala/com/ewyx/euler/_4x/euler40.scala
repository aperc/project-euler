package com.ewyx.euler._4x
import com.ewyx.euler.core.Utils

object Euler40 {

  def solve() = {
    val chConstant = for {
      i <- 1 to 200000
    } yield Utils.digits(i)

    val seq = chConstant.flatten
    List(seq(0), seq(9), seq(99), seq(999), seq(9999), seq(99999), seq(999999)).reduce(_ * _)
  }
}