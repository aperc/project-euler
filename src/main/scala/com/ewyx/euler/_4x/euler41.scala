package com.ewyx.euler._4x
import com.ewyx.euler.core.Utils

object Euler41 {
  //feel guilty for doing this.
  def solve() = {
    val primes = for {
      i <- List(1, 2, 3, 4, 5, 6, 7).permutations
      if (Utils.isPrime(Utils.digitsToInt(i)))
    } yield Utils.digitsToInt(i)
    primes.toList.max
  }
}