package com.ewyx.euler._4x

import scala.io.Source

object Euler42 {
  //cached squares.
  //doing it the hard way, with discriminants...
  val squares = for {
    i <- 1 to 50
  } yield i * i

  def disc(n: Int): Option[Int] = {
    if (squares.contains(n)) {
      val sqVal = squares.indexOf(n) + 1
      if ((-1 + sqVal) % 2 == 0) Some((-1 + sqVal) / 2) else None
    } else {
      None
    }
  }
  def delta(n: Int): Int = 1 + 4 * n

  def isTriangle(n: Int): Boolean = {
    disc(delta(n * 2)) match {
      case Some(x) => true
      case None => false
    }
  }

  def solve() = {
    val file = Source.fromFile("euler42.txt")
    val words = file.getLines().mkString.split(',').map(x => x.substring(1, x.length - 1))
    val values = words map (word => word.map(x => x.toInt - 64) reduceLeft (_ + _))
    values.filter(isTriangle).length
  }
}