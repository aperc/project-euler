/**
 *
 */
package com.ewyx.euler._4x
import com.ewyx.euler.core.Utils

import scala.BigInt
import scala.math.BigInt.int2bigInt

/**
 * @author Ewyx
 *
 * The number, 1406357289, is a 0 to 9 pandigital number because it is
 * made up of each of the digits 0 to 9 in some order,
 * but it also has a rather interesting sub-string divisibility property.
 *
 * Let d1 be the 1st digit, d2 be the 2nd digit, and so on.
 * In this way, we note the following:
 *
 * d2d3d4=406 is divisible by 2
 * d3d4d5=063 is divisible by 3
 * d4d5d6=635 is divisible by 5
 * d5d6d7=357 is divisible by 7
 * d6d7d8=572 is divisible by 11
 * d7d8d9=728 is divisible by 13
 * d8d9d10=289 is divisible by 17
 *
 * Find the sum of all 0 to 9 pandigital numbers with this property.
 *
 */
object Euler43 {

  val divs = List(17, 13, 11, 7, 5, 3, 2, 1)
  val nums = 0 to 9 toList

  def candidates(num: Int, digits: List[Int], divisor: Int): List[Int] = {
    def candidate(n: Int) = n * 100 + num / 10
    for {
      a <- digits
      if (candidate(a) % divisor) == 0 &&
        candidate(a) > 10
    } yield candidate(a)
  }

  val startingCandidates = for {
    a <- 0 to 1000 by 17
    if Utils.digits(a).distinct.length == 3
  } yield a

  def freeDigits(a: List[Int]): List[Int] = nums.diff(a.flatMap(Utils.digits(_)))

  def findNext(chain: List[Int], divisors: List[Int], acc: List[List[Int]]): List[List[Int]] = {
    if (chain.length == 8) {
      chain :: acc
    } else {
      divisors match {
        case x :: xs => {
          val chains = for {
            c <- candidates(chain.head, freeDigits(chain), x)
          } yield findNext(c :: chain, xs, acc)
          chains.flatten ::: acc
        }
        case _ => acc
      }
    }
  }
  def collapse(l: List[Int]) = l.tail.foldLeft(BigInt(l.head))((x, rest) => x * 10 + rest % 10)
  def solve() = {
    val lists = for {
      n <- startingCandidates
    } yield findNext(List(n), divs.tail, List())
    lists.flatten.map(collapse).sum
  }
}