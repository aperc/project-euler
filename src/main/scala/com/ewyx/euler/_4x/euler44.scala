package com.ewyx.euler._4x

import scala.collection.immutable.HashSet

object Euler44 {
  // Not impressive algorithmically. But I kinda like it mostly because 
  // a) I learned I can return instead of yield in 'for comprehensions', which is kinda neat
  // b) HashSets are pretty fast, I'm sure this'll be useful :) 
  def solve(): Option[Int] = {
    val a = 1 to 3000 toList
    val pents = a map (n => n * (3 * n - 1) / 2)
    val set: Set[Int] = HashSet() ++ pents
    for {
      x <- pents
      y <- pents
      if (x != y &&
        set.contains(x - y) &&
        set.contains(x + y))
    } return Some(x - y)
    None
  }
}