package com.ewyx.euler._4x

import scala.collection.immutable.HashSet
import scala.BigInt
import scala.math.BigInt.int2bigInt

object Euler45 {
  def solve() = {
    //I have a love affair with hashsets, not healthy
    val a = (1 to 200000 toList) map (BigInt(_))
    //val triangle = HashSet() ++ a map (n => n*(n+1)/2)
    val pents = HashSet() ++ a map (n => n * (3 * n - 1) / 2)
    val hex = HashSet() ++ a map (n => n * (2 * n - 1))

    val res = for {
      x <- pents
      if (hex.contains(x))
    } yield x
    res
  }
}