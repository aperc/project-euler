package com.ewyx.euler._4x
import com.ewyx.euler.core.Utils

object Euler46 {
  def findGoldbach(): Int = {
    def sumOfPrimeExists(n: Int, primes: Set[Int]): Boolean = {
      val lowerPrimes = primes.filter(_ < n)
      val sq = Stream.from(1) map (x => 2 * x * x) takeWhile (_ < n)
      for {
        p <- lowerPrimes
        if (sq.contains(n - p))
      } return true
      false
    }
    val primes = Utils.sieveOfEratosthenes(6000).toSet
    val composites = (3 to 100000 by 2) filter (!primes.contains(_))
    val squares = 1 to 1000 map (x => x * x)
    for {
      c <- composites
      if (!sumOfPrimeExists(c, primes))
    } return c
    -1
  }

  def solve() = findGoldbach
}