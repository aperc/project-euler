package com.ewyx.euler._4x
import com.ewyx.euler.core.Utils

object Euler47 {
  def checkDistincts(n: List[Int]): Boolean = {
    for {
      l <- n
      if (!(Utils.factorize(l).distinct.length == 4))
    } return false
    true
  }
  def solve(): Option[Int] = {
    val numbers = (1 to 1000000)
    for {
      n <- numbers.sliding(4)
      if (checkDistincts(n toList))
    } return Some(n(0))
    None
  }
}