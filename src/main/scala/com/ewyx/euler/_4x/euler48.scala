package com.ewyx.euler._4x

object Euler48 {
  def pow10d(base: Int, pow: Int): Long = {
    (1 to pow).foldLeft(1L)((x, y) => (x * base).toLong % 100000000000L)
  }
  def solve() = {
    (for {
      i <- 1 to 1000
    } yield pow10d(i, i)).sum % 100000000000L
  }
}