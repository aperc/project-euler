package com.ewyx.euler._4x
import com.ewyx.euler.core.Utils

object Euler49 {
  def samePermutation(i: Int, j: Int, k: Int): Boolean = {
    (Utils.digits(i).sortBy(x => x) == Utils.digits(j).sortBy(x => x)) &&
      (Utils.digits(k).sortBy(x => x) == Utils.digits(j).sortBy(x => x))
  }
  def solve() = {
    val primes = Utils.sieveOfEratosthenes(10000)
    val pSet = primes.toSet
    for {
      i <- primes.dropWhile(_ < 1000)
      j <- primes.dropWhile(_ <= i)
      if (pSet.contains(j + (j - i)) && samePermutation(i, j, j + (j - i)))
    } yield (i, j, j + (j - i))
  }
}