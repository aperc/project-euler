package com.ewyx.euler._5x
import com.ewyx.euler.core.Utils._

object Euler53 {
  //Note, implement the Pascal's Triangle solution at one point
  //val n = 23
  //val r = 10
  def greaterThan1MM(n: Int, r: Int): Boolean = {
    val numFactors = (1 to n) map factorize flatten
    val denFactors = (1 to r).toList ::: (1 to (n - r)).toList map factorize flatten
    val num = numFactors.diff(denFactors).foldLeft(1)(_ * _)
    val den = denFactors.diff(numFactors).foldLeft(1)(_ * _)
    num / den > 1000000 || num / den < 0 //heheheheh... overflow.
  }
  //greaterThan1MM(n, r)

  def solve() = {
    val results = for {
      n <- 1 to 100
      r <- (1 to 100).takeWhile(n >= _)
    } yield greaterThan1MM(n, r)

    results.filter(x => x).size
  }
}