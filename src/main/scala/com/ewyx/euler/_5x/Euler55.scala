package com.ewyx.euler._5x

import scala.BigInt

object Euler55 {
  def isPalindrome(n: BigInt) = n.toString == n.toString.reverse

  def lychrelIter(n: BigInt): Stream[BigInt] = {
    def sumWithReverse(n: BigInt) = n + BigInt(n.toString.reverse)
    lazy val lychrel: Stream[BigInt] = sumWithReverse(n) #:: lychrel.map(sumWithReverse)
    lychrel
  }
  def solve() = {
    val nonLychrel = 1 to 10000 filter (lychrelIter(_).take(50).takeWhile(x => !isPalindrome(x)).size < 50)
    10000 - nonLychrel.length
  }
}

/*
 old solution for reference
 * 
 *
 *   (1 to 49).foldLeft(sumWithReverse(n))((x, y) => if (isPalindrome(x)) x else sumWithReverse(x))
 *   1 to 10000 map (lychrelIter(_).take(49).takeUntil(isPalindrome)) filterNot(isPalindrome) length
*/ 