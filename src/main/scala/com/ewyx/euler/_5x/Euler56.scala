package com.ewyx.euler._5x

import scala.BigInt

object Euler56 {

  def digSum(x: Int, y: Int): Int = BigInt(x).pow(y).toString.map(Character.getNumericValue).sum

  def solve() = {
    (for {
      i <- 1 to 100
      j <- 1 to 100
    } yield digSum(i, j)).max
  }
}