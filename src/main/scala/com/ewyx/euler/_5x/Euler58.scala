package com.ewyx.euler._5x

import com.ewyx.euler.core.Utils._
import scala.annotation.tailrec

object Euler58 {
  val seq = Stream.from(1) map (_ * 2)
  def addSide(length: Int, start: Int): List[Int] = {
    (start to start + 4 * length by length toList).tail
  }

  @tailrec
  def ratio(start: Int, sides: Stream[Int], primeCnt: Int, total: Int): Int = {
    val break = 0.1

    if (primeCnt.toFloat / total.toFloat > break) {
      val newSpiral = addSide(sides.head, start) reverse
      val newPrimes = (newSpiral.tail.filter(_ prime)).length
      ratio(newSpiral.head, sides.tail, primeCnt + newPrimes, total + 4)
    } else {
      println(primeCnt)
      sides.head - 1
    }
  }

  def solve() = {
    ratio(addSide(2, 1).last, seq.tail, 3, 5)
  }
}