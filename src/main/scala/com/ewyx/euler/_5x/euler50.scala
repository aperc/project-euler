package com.ewyx.euler._5x

import com.ewyx.euler.core.Utils

object Euler50 {
  val primeList = Utils.sieveOfEratosthenes(1000000)
  val pSet = primeList.toSet

  val LIMIT = 1000000

  case class PrimeSequence(sum: Int, length: Int)

  def findSeq(primes: List[Int], result: PrimeSequence, current: PrimeSequence): PrimeSequence = {
    if (current.sum > LIMIT) {
      result
    } else {
      primes match {
        case x :: xs => {
          val curr = PrimeSequence(current.sum + x, current.length + 1)
          if (pSet.contains(x + current.sum)) {
            val res = PrimeSequence(x + current.sum, current.length + 1)
            findSeq(xs, res, curr)
          } else {
            findSeq(xs, result, curr)
          }
        }
        case _ => result
      }
    }
  }
  def getLongestSeq(primes: List[Int], results: List[PrimeSequence]): List[PrimeSequence] = {
    primes match {
      case x :: xs => {
        val start = PrimeSequence(x, 1)
        getLongestSeq(xs, findSeq(xs, start, start) :: results)
      }
      case _ => results
    }
  }
  def solve() = {
    getLongestSeq(primeList, List()).sortBy(_.length).reverse.head
  }
}