package com.ewyx.euler._5x

import com.ewyx.euler.core.Utils
import scala.annotation.tailrec

object Euler51 {
  val primes = Utils.sieveOfEratosthenes(1000000)
  val primeSet = primes.toSet
  type RepDigit = List[Option[Int]]

  def replacements(n: List[Int]): List[RepDigit] = {
    def replace(n: List[Int], d: Int): RepDigit = {
      n map (x => if (x == d) None else Some(x))
    }
    n.foldLeft(List[RepDigit]())((y: List[RepDigit], x: Int) => replace(n, x) :: y).distinct
  }

  def countOccurence(l: RepDigit): Int = {
    val st = if (l.head == None) 1 else 0
    val ns = for {
      i <- st to 9
    } yield Utils.digitsToInt(l.map {
      case Some(x) => x
      case None => i
    })
    ns.filter(primeSet.contains(_)).length
  }

  def replacementLength(n: Int): Int = {
    (for {
      rep <- replacements(Utils.digits(n))
    } yield countOccurence(rep)) max
  }

  def solve(): Option[Int] = {
    for {
      i <- primes
      if (replacementLength(i) == 8)
    } return Some(i)
    None
  }
}