package com.ewyx.euler._5x

import com.ewyx.euler.core.Utils
import scala.annotation.tailrec

object Euler52 {
  def isPermutated(x: Int) = (1 to 6).map(_ * x).map(Utils.digits(_).toSet).distinct.length == 1
  def solve(): Option[Int] = {
    for {
      x <- 1 to 500000
      if (isPermutated(x))
    } return Some(x)
    None
  }
}