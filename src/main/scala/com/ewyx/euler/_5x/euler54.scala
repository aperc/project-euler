package com.ewyx.euler._5x

import com.ewyx.euler.unsorted.Euler81._

import scala.io.Source

object Euler54 {
  //setup the types
  object Suit extends Enumeration {
    type Suit = Value
    val H, C, S, D = Value
  }
  import Suit._

  //class for cards
  case class Card(value: String, suit: Suit) extends Ordered[Card] {
    def toIntValue: Int = {
      this.value match {
        case "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" => this.value.toInt
        case "T" => 10
        case "J" => 11
        case "Q" => 12
        case "K" => 13
        case "A" => 14
      }
    }
    def compare(that: Card) = this.toIntValue - that.toIntValue
  }
  //hand is a custom type, for clarity
  type Hand = List[Card]
  //map to pure values
  def handValues(hand: Hand) = hand.map(_.toIntValue)
  //grouping cards by values
  def groupCards(hand: Hand): List[Int] = {
    handValues(hand).groupBy(x => x).values.map(_.length).toList.sortBy(x => x).reverse
  }
  //handle all the types of hands
  def isFlush(hand: Hand): Boolean = hand.map(_.suit).distinct.length == 1

  def isStraight(hand: Hand): Boolean = {
    (hand.map(_.toIntValue).max - hand.map(_.toIntValue).min == 4) &&
      (hand.map(_.toIntValue).distinct.length == 5)
  }
  def isStraightFlush(hand: Hand): Boolean = isFlush(hand) && isStraight(hand)
  def isRoyalFlush(hand: Hand): Boolean = isFlush(hand) && isStraight(hand) && handValues(hand).max == 14
  def isPair(hand: Hand): Boolean = groupCards(hand) == List(2, 1, 1, 1)
  def isTwoPairs(hand: Hand): Boolean = groupCards(hand) == List(2, 2, 1)
  def isThreeOfAKind(hand: Hand): Boolean = groupCards(hand) == List(3, 1, 1)
  def isFourOfAKind(hand: Hand): Boolean = groupCards(hand) == List(4, 1)
  def isFullHouse(hand: Hand): Boolean = groupCards(hand) == List(3, 2)

  def groupByValue(hand: Hand) = {
    //uh... yeah, sure why not... we sort cards by how many are there, and value, 
    //for comparing equal strength hands 
    hand.groupBy(x => x.value).values.toList //first we group by card value
      .sortBy(x => x.head.toIntValue) // first we sort by value e.g. L(L(10,10),L(9,9,9))
      .sortBy(x => x.length) // then we sort by the amount of cards L(L(9,9,9), L(10,10))
      .reverse.flatten.map(_.toIntValue) //we flatten and conver them to int value
    //this way we can be sure that we'll be comparing the cards in the correct order
    //to deduce the highest card (this could be a single card, a pair, a poker, or a three of a kind).
  }

  def evalHand(hand: Hand): Int = {
    hand match {
      case x if isRoyalFlush(x) => 10
      case x if isStraightFlush(x) => 9
      case x if isFourOfAKind(x) => 8
      case x if isFullHouse(x) => 7
      case x if isFlush(x) => 6
      case x if isStraight(x) => 5
      case x if isThreeOfAKind(x) => 4
      case x if isTwoPairs(x) => 3
      case x if isPair(x) => 2
      case _ => 1
    }
  }

  def p1Wins(p1: Hand, p2: Hand): Boolean = {
    val h1 = evalHand(p1)
    val h2 = evalHand(p2)
    if (h1 > h2) {
      true
    } else if (h2 > h1) {
      false
    } else {
      val h1Values = groupByValue(p1)
      val h2Values = groupByValue(p2)
      val diffCards = h1Values.zip(h2Values).filterNot(x => x._1 == x._2).head
      diffCards._1 > diffCards._2
    }
  }

  def parseCard(s: String): Card = {
    new Card(s.substring(0, 1), Suit.withName(s.substring(1, 2)))
  }
  def parseLine(s: String): List[Card] = s.split(" ").toList.map(parseCard)

  def solve() = {
    val file = Source.fromInputStream(getClass().getResourceAsStream("/poker.txt"))
    val hands = file.getLines()
    val wins = for {
      h <- hands.map(parseLine(_))
    } yield p1Wins(h.take(5), h.drop(5))
    wins.toList.filter(x => x).length
  }
}