package com.ewyx.euler._5x

import scala.math.BigInt.int2bigInt

object Euler57 {
  case class Fraction(num: BigInt, den: BigInt)
  //the first iteration is actually the 2nd.
  def sqrtExpansion(d: Fraction, iterations: Int): Fraction = {
    iterations match {
      case 0 => d
      case _ => sqrtExpansion(Fraction(2 * d.num + d.den, d.num), iterations - 1)
    }
  }
  def getRes(n: Int): Fraction = {
    val res = sqrtExpansion(Fraction(5, 2), n)
    Fraction(res.den + res.num, res.num)
  }

  def solve() = {
    (for {
      i <- (1 to 998).map(getRes(_))
      if (i.num.toString.length > i.den.toString.length)
    } yield i) length
  }
}