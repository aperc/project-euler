package com.ewyx.euler._5x

import com.ewyx.euler._

object Euler59 {

  /*
	 * 
	 * CLEAN THIS UP!
	 * 
	 */
  val str = loadFile("cipher1.txt").toString()

  val values = str.split(',').map(_.toInt)
  def decrypt(key: List[Int]) = {
    values.zip(Stream.continually(key).flatten)
      .map(x => x._1 ^ x._2)
    //.map(_.toChar).toList.foldLeft("")((x,y) => y + x) reverse
  }
  def freqs(key: List[Int]) = {
    values.zip(Stream.continually(key).flatten)
      .map(x => x._1 ^ x._2)
      .map(_.toChar).toList.foldLeft("")((x, y) => y + x)
      .groupBy(x => x)
      .map(x => (x._1, x._2.length)).toSeq
      .sortBy(_._2).reverse.map(_._1)
  }
  for {
    i <- List(('c', 'o', 'd'), ('g', 'g', 'd'), ('g', 'o', 'd'), ('g', 'o', 'j'), ('i', 'o', 'd'), ('j', 'o', 'd'), ('v', 'o', 'd'))
  } yield decrypt(List(i._1, i._2, i._3)).take(15)

  decrypt(List('g', 'o', 'd')) sum
  /*val solutions = for {
		a <- 'a' to 'z'
		b <- 'a' to 'z'
		c <- ('a' to 'z')
		d = freqs(List(a.toInt, b.toInt, c.toInt)) take(8)
		if(d.contains('e') && d.contains('a') && d.contains('t') && d.contains('o') && d.contains('i'))
	} yield (a,b,c)*/
}