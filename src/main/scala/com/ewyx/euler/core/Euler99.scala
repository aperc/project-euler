package com.ewyx.euler.core

import com.ewyx.euler._
import scala.math._

object Euler99 {

  def compare(a: List[Int], b: List[Int]): Boolean = {
    (a(1) * log(a.head)) > (b(1) * log(b.head))
  }

  def solve() = {
    val file = loadFile("p099_base_exp.txt").getLines.toList
    val biggest = (for {
      line <- file
    } yield line split (',') map (_.toInt) toList).toList.sortWith(compare)
    file.indexOf(biggest.head(0) + "," + biggest.head(1))
  }
}