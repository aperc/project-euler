package com.ewyx.euler.core

trait EulerProblem[T] {
  def solve(): T
}