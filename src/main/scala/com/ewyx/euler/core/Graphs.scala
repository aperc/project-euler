package com.ewyx.euler.core

import scala.collection._

object Graphs {
  type Graph = List[Vertex]

  class Vertex(v: Int) {
    var value: Int = v
    var edges: List[Vertex] = List()

    def connect(v: Vertex) {
      edges = v :: edges
    }
  }
  def dijkstra(g: Graph, source: Vertex) = {
    var dist = new mutable.HashMap[Vertex, Int]()
    dist.put(source, source.value)
    var visited = new mutable.HashSet[Vertex]()
    def checkNeighbours(u: Vertex, g: Graph) {
      for {
        v <- u.edges
        if (g.contains(v))
      } {
        dist.put(v, (dist.getOrElse(u, Int.MaxValue - 10000) + v.value) min dist.getOrElse(v, Int.MaxValue - 10000))
      }
    }

    def search(g: Graph): Graph = {
      g match {
        case x :: xs => {
          val nextVertex = g.sortBy(x => dist.getOrElse(x, Int.MaxValue)).head
          checkNeighbours(nextVertex, g)
          search(g.filter(!_.equals(nextVertex)))
        }
        case _ => g
      }
    }
    search(g)
    dist
  }
}