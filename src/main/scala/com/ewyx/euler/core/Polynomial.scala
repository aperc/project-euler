package com.ewyx.euler.core

case class PolynomialElement(a: Int, power: Int)

object PolynomialUtils {
  def collapse(a: Seq[PolynomialElement]) = {
    a.groupBy(_.power)
      .map {
        case (key, values) => PolynomialElement(values.map(_.a).sum, key)
      }
  }

  def multiply(cutoff: Int)(a: Seq[PolynomialElement], b: Seq[PolynomialElement]): Seq[PolynomialElement] = {
    collapse(for {
      ax <- a
      bx <- b
      if (ax.power + bx.power <= cutoff)
    } yield PolynomialElement(ax.a * bx.a, ax.power + bx.power)).toSeq
  }
}
