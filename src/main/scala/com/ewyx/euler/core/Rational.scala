package com.ewyx.euler.core

case class Rational(numerator: BigInt, denominator: BigInt)

object Rational {
  implicit object RationalIsNumeric extends Numeric[Rational] with Ordering[Rational] {
    def compare(x: Rational, y: Rational): Int = (x.numerator * y.denominator - x.denominator * y.numerator).signum

    def fromInt(x: Int): Rational = Rational(x, 1)

    def minus(x: Rational, y: Rational): Rational =
      Rational(
        x.denominator * y.numerator - y.denominator * x.numerator,
        x.denominator * y.denominator
      )

    def negate(x: Rational): Rational = Rational(-x.denominator, x.numerator)

    def plus(x: Rational, y: Rational): Rational = {
      Rational(
        x.denominator * y.numerator + y.denominator * x.numerator,
        x.denominator * y.denominator
      )
    }

    def times(x: Rational, y: Rational): Rational = {
      Rational(x.numerator * y.numerator, x.denominator * y.denominator)
    }

    def toDouble(x: Rational): Double = x.numerator.toDouble / x.denominator.toDouble

    def toFloat(x: Rational): Float = x.numerator.toFloat / x.denominator.toFloat

    def toInt(x: Rational): Int = (x.numerator / x.denominator).toInt

    def toLong(x: Rational): Long = x.numerator.toLong / x.denominator.toLong
  }
  def apply(numerator: BigInt, denominator: Rational): Rational = {
    Rational.RationalIsNumeric.times(Rational(numerator, 1), Rational(denominator.denominator, denominator.numerator))
  }
}
