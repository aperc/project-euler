package com.ewyx.euler.core

import scala.util.parsing.combinator.RegexParsers
import scala.collection.immutable.HashMap

object RomanNumeralParser extends RegexParsers {
  private val numValue = HashMap(
    "I" -> 1,
    "V" -> 5,
    "X" -> 10,
    "L" -> 50,
    "C" -> 100,
    "D" -> 500,
    "M" -> 1000,
    "IV" -> 4,
    "IX" -> 9,
    "XL" -> 40,
    "XC" -> 90,
    "CD" -> 400,
    "CM" -> 900
  )
  private val orderedNumerals = numValue.toList.sortBy(-_._2)
  def identifier = "M|CM|D|CD|XC|C|L|XL|X|IX|V|IV|I".r ^^ { numValue(_) }
  def expr = identifier*

  def apply(in: String) = parseAll(expr, in)

  def toRomanNumeral(n: Int): String = {
    def writeNumber(acc: String, remainder: Int): String = {
      if (remainder == 0) {
        acc
      } else {
        val highestFitting = orderedNumerals.dropWhile(x => remainder < x._2)
        writeNumber(acc + highestFitting.head._1, remainder - highestFitting.head._2)
      }
    }
    writeNumber("", n)
  }
}