package com.ewyx.euler.core

import Utils._

class SquareRoot(val n: Int) {
  def isPerfect: Boolean = this.n ** 2 == nearestPerfectRoot

  private val squares = Stream.from(1).map(x => (x, x ** 2)).takeWhile(_._1 <= 10000)

  def nearestPerfectRoot: Int = squares.takeWhile(_._2 < this.n).last._1

  def rootPeriod = {
    def nextStep(a0: Int, S: Int, a: Int, m: Int, d: Int): (Int, Int, Int) = {
      val m_n = d * a - m
      val d_n = (S - (m_n ** 2)) / d
      val a_n = (a0 + m_n) / d_n
      (a_n, m_n, d_n)
    }
    val d0 = 1
    val m0 = 0
    val a0 = nearestPerfectRoot

    lazy val stream: Stream[(Int, Int, Int)] = nextStep(a0, this.n, a0, m0, d0) #:: stream.map(x => nextStep(a0, this.n, x._1, x._2, x._3))

    stream.map(_._1).takeWhile(_ != a0 * 2).toList ++ List(a0 * 2)
  }
}