package com.ewyx.euler.core

import scala.collection.mutable.BitSet
import scala.annotation.tailrec
import scala.collection.immutable.Stream.consWrapper
import scala.collection.mutable.HashMap
/**
 * Random utils
 */
object Utils {

  implicit class IntExt(n: Int) {
    def prime: Boolean = isPrime(n)
    def **(exp: Int) = math.pow(n, exp).toInt
    def digitList: List[Int] = digits(n)
    /*
        Triangle 	  	P3,n=n(n+1)/2 	  	1, 3, 6, 10, 15, ...
		Square 	  		P4,n=n2 	  		1, 4, 9, 16, 25, ...
		Pentagonal 	  	P5,n=n(3n-1)/2 	  	1, 5, 12, 22, 35, ...
		Hexagonal 	  	P6,n=n(2n-1) 	  	1, 6, 15, 28, 45, ...
		Heptagonal 	  	P7,n=n(5n-3)/2 	  	1, 7, 18, 34, 55, ...
		Octagonal 	  	P8,n=n(3n-2) 	  	1, 8, 21, 40
     */
    def triangle: Int = n * (n + 1) / 2
    def square: Int = n * n
    def pentagonal: Int = n * (3 * n - 1) / 2
    def hexagonal: Int = n * (2 * n - 1)
    def heptagonal: Int = n * (5 * n - 3) / 2
    def octagonal: Int = n * (3 * n - 2)
  }
  //make these into proper streams
  def primes(): Stream[Int] = {
    2 #:: 3 #:: 5 #:: 7 #:: primesAcc(Stream.from(2).filter(x => x % 2 != 0 && x % 3 != 0 && x % 5 != 0 && x % 7 != 0))
  }

  def primesAcc(candidates: Stream[Int]): Stream[Int] = {
    candidates.head #:: primesAcc(candidates.tail.filter(_ % candidates.head != 0))
  }

  def isPrime(n: Int): Boolean = {
    if (n > 1) {
      (2 to math.sqrt(n).toInt dropWhile (n % _ != 0)).isEmpty
    } else {
      false
    }
  }
  /**
   * Rewrite to functional :/
   */
  var factorizedHashes: HashMap[Int, List[Int]] = new HashMap[Int, List[Int]]()
  val primeList = Utils.sieveOfEratosthenes(1000000)
  def factorize(number: Int): List[Int] = {
    factorizedHashes.get(number) match {
      case Some(x) => x
      case None =>
        var primes = primeList
        var result: List[Int] = List()
        var n = number;
        while (n >= primes.head) {
          if (n % primes.head == 0) {
            result = primes.head :: result
            n = n / primes.head
          } else {
            primes = primes.tail
          }
        }
        factorizedHashes.put(number, result)
        result
    }
  }

  //Sieve of Eratosthenes
  def sieveOfEratosthenes(limit: Int): List[Int] = {
    val primes = BitSet.empty.par ++ (2 +: (3 to limit by 2))
    for {
      candidate <- 3 until Math.sqrt(limit).toInt
      if primes contains candidate
    } primes --= candidate * candidate to limit by candidate
    primes.toList.sorted
  }

  /**
   * Binary GCD, only works on positive integers.
   * takes two integers, return their gcd
   */
  def gcd(i: Int, j: Int): Int = {
    if (i == j) {
      i
    } else if (i == 0) j
    else if (j == 0) i
    else {
      if ((~i & 1) != 0) {
        if ((j & 1) != 0) {
          gcd(i >> 1, j)
        } else {
          gcd(i >> 1, j >> 1) << 1
        }
      } else if ((~j & 1) != 0) gcd(i, j >> 1)
      else if (i > j) gcd((i - j) >> 1, j)
      else gcd((j - i) >> 1, i)
    }
  }

  //Checks if two numbers are coprimes
  def coprimes(i: Int, j: Int): Boolean = gcd(i, j) == 1

  def divisorSum(divisors: List[(Int, Int)]): Int = {
    def delta(div: Int, pow: Int): Int = {
      ((math.pow(div, pow + 1) - 1) / (div - 1)).toInt
    }
    divisors.map(x => delta(x._1, x._2)).foldLeft(1)(_ * _)
  }

  def sumOfProperDivisors(number: Int): Int = {
    def factors = Utils.factorize(number).groupBy(x => x)
    factors.map(x => (x._1, x._2.length)).toList
    Utils.divisorSum(factors.map(x => (x._1, x._2.length)).toList) - number
  }

  @tailrec
  def digits(n: Int, d: List[Int] = List()): List[Int] = {
    n match {
      case n: Int if (n / 10 == 0) => n :: d
      case _ => digits(n / 10, (n % 10 :: d))
    }
  }

  @tailrec
  def digitsToInt(d: List[Int], n: Int = 0): Int = {
    d match {
      case x :: xs => digitsToInt(xs, n + x * (**(10, d.length - 1)))
      case _ => n
    }
  }

  //rewrite to a faster solution that doesn't use recursion.
  def fib(n: Int): Int = n match {
    case 0 | 1 => 1
    case _ => fib(n - 1) + fib(n - 2)
  }

  def factorial(n: Int): Int = if (n == 0) 1 else (1 to n) reduce (_ * _)

  def sum(n: IndexedSeq[Int]): Int = n reduce (_ + _)
  //stolen from SO, neat timing function
  def time[A](f: => A) = {
    val s = System.nanoTime
    val ret = f
    println("time: " + (System.nanoTime - s) / 1e6 + "ms")
    ret
  }

  def **(base: Int, pow: Int): Int = math.pow(base, pow).toInt

  def memoize[A, B](f: A => B): A => B = {
    val map = HashMap[A, B]()
    def memoized(a: A): B = map.getOrElseUpdate(a, f(a))
    memoized _
  }
}