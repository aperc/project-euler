package com.ewyx

import scala.io.Source

package object euler {
  def loadFile(name: String) = {
    (getClass.getResourceAsStream _ andThen Source.fromInputStream)("/" + name)
  }
}
