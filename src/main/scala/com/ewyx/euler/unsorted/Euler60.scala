package com.ewyx.euler.unsorted

import com.ewyx.euler.core.Utils._

object Euler60 {
  //precalculate primes, for a bit of a speedup
  val primes = sieveOfEratosthenes(1000000).toSet
  //pool of primes, to generate sets from.
  val primesShort = sieveOfEratosthenes(10000)

  def isConcatenatedPrime(a: Int, b: Int): Boolean = {
    val c1 = a * (10 ** b.toString.length) + b
    val c2 = b * (10 ** a.toString.length) + a
    if (c1 > 1000000 || c2 > 1000000)
      (c1 prime) && (c2 prime)
    else
      primes.contains(c1) && primes.contains(c2)
  }

  def concatanableSet(s: Set[Int], n: Int): Boolean = {
    val a = for {
      a <- s
      if (!isConcatenatedPrime(a, n))
    } return false
    true
  }

  //weird way to do a breadth first search...
  def firstConcatanablePrimeSet(): List[Int] = {
    for {
      a <- primesShort
      b <- primesShort.dropWhile(_ <= a).filter(x => concatanableSet(Set(a), x))
      c <- primesShort.dropWhile(_ <= b).filter(x => concatanableSet(Set(a, b), x))
      d <- primesShort.dropWhile(_ <= c).filter(x => concatanableSet(Set(a, b, c), x))
      e <- primesShort.dropWhile(_ <= d).filter(x => concatanableSet(Set(a, b, c, d), x))
    } return List(a, b, c, d, e)
    List()
  }

  def solve(): Int = {
    firstConcatanablePrimeSet sum
  }
}