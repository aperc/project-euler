package com.ewyx.euler.unsorted

object Euler61 {
  /**
   * Triangle, square, pentagonal, hexagonal, heptagonal, and octagonal numbers are all figurate (polygonal) numbers and are generated by the following formulae:
   * Triangle 	  	P3,n=n(n+1)/2 	  	1, 3, 6, 10, 15, ...
   * Square 	  	P4,n=n2 	  		1, 4, 9, 16, 25, ...
   * Pentagonal 	P5,n=n(3n−1)/2 	  	1, 5, 12, 22, 35, ...
   * Hexagonal 	  	P6,n=n(2n−1) 	  	1, 6, 15, 28, 45, ...
   * Heptagonal 	P7,n=n(5n−3)/2 	  	1, 7, 18, 34, 55, ...
   * Octagonal 	  	P8,n=n(3n−2) 	  	1, 8, 21, 40, 65, ...
   *
   * The ordered set of three 4-digit numbers: 8128, 2882, 8281, has three interesting properties.
   *
   * The set is cyclic, in that the last two digits of each number is the first two digits of the next number (including the last number with the first).
   * Each polygonal type: triangle (P3,127=8128), square (P4,91=8281), and pentagonal (P5,44=2882), is represented by a different number in the set.
   * This is the only set of 4-digit numbers with this property.
   *
   * Find the sum of the only ordered set of six cyclic 4-digit numbers for which each polygonal type:
   * triangle, square, pentagonal, hexagonal, heptagonal, and octagonal, is represented by a different number in the set.
   *
   */

  //helper case class to make things more sane
  private case class SetCandidate(set: List[Int], pools: List[List[Int]])

  private def tri(n: Int): Int = n * (n + 1) / 2
  private def sq(n: Int): Int = n * n
  private def pent(n: Int): Int = n * (3 * n - 1) / 2
  private def hex(n: Int): Int = n * (2 * n - 1)
  private def hept(n: Int): Int = n * (5 * n - 3) / 2
  private def oct(n: Int): Int = n * (3 * n - 2)

  //returns a new candidate, with the taken number pool removed
  private def findCandidates(n: SetCandidate): List[SetCandidate] = {
    n.pools match {
      case List() => List[SetCandidate]()
      case x => for {
        list <- x
        element <- list
        if (n.set.head / 100 == element % 100)
      } yield SetCandidate(element :: n.set, n.pools.filter(_ != list))
    }
  }

  private def findSet(candidates: List[SetCandidate]): List[SetCandidate] = {
    candidates.head.pools match {
      case x :: xs => {
        val newLevel: List[SetCandidate] = (for {
          c <- candidates
        } yield findCandidates(c)).flatten.toList
        findSet(newLevel)
      }
      case _ => candidates
    }
  }

  def solve() = {
    val fns = List(tri _, sq _, pent _, hex _, hept _, oct _)
    //generate all pools (numbers that we can use)
    val numberPools = for {
      f <- fns
    } yield Stream.from(1)
      .map(f(_))
      .dropWhile(_ < 1000)
      .takeWhile(_ < 10000)
      .toList

    val startingPool = numberPools.head.map(x => SetCandidate(List(x), numberPools.tail))
    //Should find all sets that can form a 'chain'
    findSet(startingPool)
      .filter(x => x.set.head / 100 == x.set.last % 100) //filter the cyclical ones
      .head.set.sum //take the first element, and sum the set
  }
}