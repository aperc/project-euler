package com.ewyx.euler.unsorted

import scala.BigInt

object Euler62 {

  def solve() = {
    def sortedCubeDigits(x: Int) = {
      BigInt(x).pow(3).toString
        .map(Character.getNumericValue)
        .sortWith(_ < _)
    }

    val cubes = (for {
      i <- 1 to 10000
    } yield i)

    val smallestN = cubes.groupBy(sortedCubeDigits(_))
      .toList.sortBy(x => x._2.length)
      .filter(_._2.length == 5)
      .flatMap(_._2).min

    BigInt(smallestN).pow(3)
  }
}