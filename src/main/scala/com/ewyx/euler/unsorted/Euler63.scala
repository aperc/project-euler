package com.ewyx.euler.unsorted

import scala.BigInt
import scala.math.BigInt.int2bigInt

object Euler63 {
  def solve() = {
    val numbers = for {
      i <- BigInt(1) to 100
      j <- 1 to 40 //powers
      if ((i pow (j)).toString.length == j)
    } yield (i pow (j))

    numbers.toSet.size
  }
}