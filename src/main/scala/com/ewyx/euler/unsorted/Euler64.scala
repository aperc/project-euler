package com.ewyx.euler.unsorted

import com.ewyx.euler.core.EulerProblem
import com.ewyx.euler.core.Utils._

class Euler64 extends EulerProblem[Int] {
  private val squares = Stream.from(1).takeWhile(_ <= 10000).map(x => x ** 2)

  private def rootPeriod(S: Int) = {
    def nearestPerfectRoot(n: Int): Int = squares.takeWhile(_ < n).length

    def nextStep(a0: Int, S: Int, a: Int, m: Int, d: Int): (Int, Int, Int) = {
      val m_n = d * a - m
      val d_n = (S - (m_n ** 2)) / d
      val a_n = (a0 + m_n) / d_n
      (a_n, m_n, d_n)
    }
    val d0 = 1
    val m0 = 0
    val a0 = nearestPerfectRoot(S)

    lazy val stream: Stream[(Int, Int, Int)] = nextStep(a0, S, a0, m0, d0) #:: stream.map(x => nextStep(a0, S, x._1, x._2, x._3))

    stream.map(_._1).takeWhile(_ != a0 * 2).length + 1
  }

  def solve() = {
    val irrationalRoots = (2 to 10000).diff(squares)
    irrationalRoots.map(rootPeriod).count(_ % 2 == 1)
  }
}
