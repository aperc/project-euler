package com.ewyx.euler.unsorted

import com.ewyx.euler.core.Rational
import com.ewyx.euler.core.Rational.RationalIsNumeric._
import com.ewyx.euler.core.EulerProblem

class Euler65 extends EulerProblem[Int] {

  type R = Rational
  def invert(r: R): R = Rational(r.denominator, r.numerator)

  def solve = {
    val stream = Stream.from(2, 2)
    val start = List(1, 2, 1)
    lazy val e: Stream[R] = Stream
      .continually(start)
      .zip(stream)
      .flatMap(x => List(1, x._2, 1))
      .map((x: Int) => Rational(x, 1))

    val res = e
      .take(99)
      .reverse
      .reduceLeft((acc, x) => invert(acc) + x)

    (Rational(2, 1) + invert(res)).numerator.toString.map(_.asDigit).sum
  }
}