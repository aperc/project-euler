package euler

import com.ewyx.euler._
import com.ewyx.euler._1x.Euler18

object Euler67 {
  val source = loadFile("triangle.txt")
  val triangle = for {
    line <- source.getLines()
  } yield (line split (' ') toList) map (_.toInt)

  def solve() = {
    triangle reduce (Euler18.merge) max
  }
}