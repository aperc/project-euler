package com.ewyx.euler.unsorted

object Euler68 {
  val maxN = 10
  type Ring = List[Node]
  case class Node(a: Int, b: Int, c: Int) {
    def sum = a + b + c
    def toList = List(a, b, c)
    def strRep = a.toString + b.toString + c.toString
    def value = Integer.parseInt(a.toString + b.toString + c.toString)
  }

  def candidates(s: List[Node]) = {
    val elements = s.flatMap(_ toList)
    s match {
      case List() => for {
        a <- 1 to maxN
        b <- 1 to maxN
        c <- 1 to maxN
        if (a != b && a != c && b != c)
      } yield Node(a, b, c)

      case x if x.length < 4 => for {
        a <- 1 to maxN
        b <- 1 to maxN
        if (a + b + x.head.c == x.head.sum &&
          !elements.contains(a) && !elements.contains(b) && a != b)
      } yield Node(a, x.head.c, b)

      case x if x.length == 4 => for {
        a <- 1 to maxN
        if (x.last.b + x.head.c + a == x.head.sum &&
          a != x.head.c && a != x.last.b && !elements.contains(a))
      } yield Node(a, x.head.c, x.last.b)
    }
  }

  def correctRotation(l: List[String]): String = {
    val index = l.indexOf(l.min)
    val lists = l.splitAt(index)
    (lists._2 ::: lists._1) map (_.toString) reduceLeft (_ + _)
  }

  def step(l: List[Ring], n: Int): List[Ring] = {
    l.map(x => candidates(x).map(_ :: x)) //create a list of all possible candidates
      .flatten
      .filter(_.length == n) //remove 
  }

  def solve() = {
    val initialCandidates: List[Ring] = candidates(List()).toList.map(List(_))
    val solutions = (2 to 5).foldLeft(initialCandidates)((x, y) => step(x, y)).map(x => x map (_ strRep))
    solutions.map(x => correctRotation(x.reverse)) //find the correct rotations
      .sorted
  }
}