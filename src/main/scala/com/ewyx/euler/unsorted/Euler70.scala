package com.ewyx.euler.unsorted

import com.ewyx.euler.core.{ Utils, EulerProblem }

class Euler70 extends EulerProblem[Int] {
  private def isPermutation(x: Int, y: Int) = Utils.digits(x).sorted == Utils.digits(y).sorted

  private def eulersProdFormula(factors: List[Int]): Double = {
    (for {
      p <- factors
    } yield 1 - 1 / p.toDouble).product * factors.product
  }

  private val Limit = 10000000

  private val primes = Utils.sieveOfEratosthenes(Limit / 3)

  override def solve(): Int = (for {
    x <- primes
    y <- primes.takeWhile(prime => prime * x < Limit)
    phi = Math.round(eulersProdFormula(List(x, y)))
    if isPermutation(x * y, phi.toInt) && x != y
  } yield (x * y, (x * y).toDouble / phi)).sortBy(_._2).head._1
}
