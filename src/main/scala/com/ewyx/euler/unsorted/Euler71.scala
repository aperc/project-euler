package com.ewyx.euler.unsorted

import com.ewyx.euler.core.{ Rational, EulerProblem }
import com.ewyx.euler.core.Rational.RationalIsNumeric._

class Euler71(max: Rational = Rational(3, 7), limit: Int = 1000000) extends EulerProblem[BigInt] {

  override def solve(): BigInt = {
    def incNum(r: Rational) = r.copy(numerator = r.numerator + 1)
    def incDen(r: Rational) = r.copy(denominator = r.denominator + 1)

    def incRational(r: Rational): Rational = {
      if (incNum(r) >= max) {
        incDen(r)
      } else {
        incNum(r)
      }
    }

    def createFractions(curr: Rational, best: Rational): Rational = {
      val newBest = if (curr > best) curr else best
      if (curr.denominator < limit) {
        createFractions(incRational(curr), newBest)
      } else {
        best
      }
    }

    createFractions(Rational(0, 1), Rational(0, 1)).numerator
  }
}
