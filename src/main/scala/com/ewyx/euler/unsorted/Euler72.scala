package com.ewyx.euler.unsorted

import com.ewyx.euler.core.{ Utils, EulerProblem }

class Euler72 extends EulerProblem[Long] {

  def solve() = {
    def eulersProdFormula(n: Int): Float = {
      val factors = Utils.factorize(n).distinct
      ((for {
        p <- factors
      } yield (1 - 1 / p.toFloat)) reduce (_ * _)) * n
    }

    (for {
      i <- 2 to 1000000
    } yield Math.round(eulersProdFormula(i)).toLong).sum
  }
}
