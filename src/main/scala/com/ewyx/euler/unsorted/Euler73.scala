package com.ewyx.euler.unsorted

import com.ewyx.euler.core.{ EulerProblem, Utils }

class Euler73 extends EulerProblem[Int] {

  /**
   * The paper on project euler suggests also looking into Farey sequences in order
   * to get a faster algorithm, Farey sequences generate reduced fractions (and by extension coprimes)
   */

  private def getMinMax(x: Int): (Int, Int) = {
    val min = Math.ceil(x / 3.0).toInt
    val max = Math.floor(x / 2.0).toInt
    (min, max)
  }

  val limit = 12000
  def solve(): Int = {
    (for {
      d <- 4 to limit
      m = getMinMax(d)
      n <- m._1 to m._2
      if Utils.gcd(n, d) == 1
    } yield (n, d)).length
  }
}
