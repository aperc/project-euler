package com.ewyx.euler.unsorted

import com.ewyx.euler.core.{ Utils, EulerProblem }

/*
    The number 145 is well known for the property that the sum of the factorial of its digits is equal to 145:
    1! + 4! + 5! = 1 + 24 + 120 = 145

    Perhaps less well known is 169, in that it produces the longest chain of numbers that link back to 169; it turns out
    that there are only three such loops that exist:
    169 → 363601 → 1454 → 169
    871 → 45361 → 871
    872 → 45362 → 872

    It is not difficult to prove that EVERY starting number will eventually get stuck in a loop. For example,
    69 → 363600 → 1454 → 169 → 363601 (→ 1454)
    78 → 45360 → 871 → 45361 (→ 871)
    540 → 145 (→ 145)

    Starting with 69 produces a chain of five non-repeating terms, but the longest non-repeating chain with a starting
    number below one million is sixty terms.

    How many chains, with a starting number below one million, contain exactly sixty non-repeating terms?
*/
class Euler74 extends EulerProblem[Int] {
  private def chain(x: List[Int]) = {
    def factorial(x: Int) = (1 to x).product
    def next(x: List[Int]) = Utils.digits(x.map(factorial).sum)

    def inner(x: List[Int], acc: List[List[Int]] = List()): List[List[Int]] = {
      if (acc.contains(x) || acc.length > 60) {
        acc
      } else {
        (acc ++ inner(next(x), x :: acc)).distinct
      }
    }
    inner(x.sorted)
  }

  private def chainsOfOrder(order: Int) = {
    val combinations = Stream
      .continually(0 to 9)
      .take(order)
      .flatten
      .combinations(order)

    combinations.foldLeft(Map[List[Int], List[List[Int]]]()) { (acc, el) =>
      val ch = chain(el.toList)
      acc + (el.toList -> ch)
    }.filter {
      case (key, chain) =>
        chain.length == 60
    }.keys
  }

  override def solve(): Int = (1 to 6)
    .flatMap(chainsOfOrder)
    .map(_.permutations.count(_.head != 0))
    .sum
}
