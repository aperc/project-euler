package com.ewyx.euler.unsorted

import com.ewyx.euler.core.Utils
/**
 *
 * It turns out that 12 cm is the smallest length of wire that can be bent to form an
 * integer sided right angle triangle in exactly one way, but there are many more examples.
 *
 * 12 cm: (3,4,5)
 * 24 cm: (6,8,10)
 * 30 cm: (5,12,13)
 * 36 cm: (9,12,15)
 * 40 cm: (8,15,17)
 * 48 cm: (12,16,20)
 *
 * In contrast, some lengths of wire, like 20 cm, cannot be bent to form an integer
 * sided right angle triangle, and other lengths allow more than one solution to be found;
 * for example, using 120 cm it is possible to form exactly three different integer sided
 * right angle triangles.
 *
 * 120 cm: (30,40,50), (20,48,52), (24,45,51)
 *
 * Given that L is the length of the wire, for how many values of L ≤ 1,500,000 can exactly
 * one integer sided right angle triangle be formed?
 *
 */

object Euler75 {

  def solve() = {
    val limit = 1500000
    def euclidsFormula(k: Int, m: Int, n: Int): (Int, Int, Int) = {
      (k * (m * m - n * n), k * (2 * m * n), k * (m * m + n * n))
    }

    def tupleSum(x: (Int, Int, Int)): Int = x._1 + x._2 + x._3

    val triples = for {
      m <- 1 to 1300
      n <- (1 to limit).takeWhile(x => x < m && tupleSum(euclidsFormula(1, m, x)) <= limit)
      k <- (1 to limit).takeWhile(k => tupleSum(euclidsFormula(k, m, n)) <= limit)
      if ((m - n) % 2 == 1 && Utils.coprimes(m, n))
    } yield euclidsFormula(k, m, n)

    triples.distinct
      .map(tupleSum)
      .filter(_ <= limit)
      .groupBy(x => x)
      .count(_._2.length == 1)
  }
}