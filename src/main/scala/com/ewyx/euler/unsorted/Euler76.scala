package com.ewyx.euler.unsorted

import com.ewyx.euler.core.{ EulerProblem, PolynomialElement, PolynomialUtils }

/**
 *
 *
 * It is possible to write five as a sum in exactly six different ways:
 *
 * 4 + 1
 * 3 + 2
 * 3 + 1 + 1
 * 2 + 2 + 1
 * 2 + 1 + 1 + 1
 * 1 + 1 + 1 + 1 + 1
 *
 * How many different ways can one hundred be written as a sum of at least two positive integers?
 *
 * Useful links:
 * - https://en.wikipedia.org/wiki/Partition_%28number_theory%29#Generating_function
 * - https://www.youtube.com/watch?v=bfFQLSIuQQk (Explanation of generating functions)
 */
class Euler76 extends EulerProblem[Int] {

  def solve() = {
    def stepPolynomial(step: Int, max: Int) = {
      for {
        x <- 0 to 100
        y = x * step
        if (y <= max)
      } yield PolynomialElement(1, y)
    }

    val polynomials = for (i <- 1 to 100)
      yield stepPolynomial(i, 100)

    polynomials
      .foldLeft(Seq(PolynomialElement(1, 0)))(PolynomialUtils.multiply(100))
      .sortBy(_.power)
      .last.a - 1
  }
}
