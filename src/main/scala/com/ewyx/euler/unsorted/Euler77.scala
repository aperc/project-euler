package com.ewyx.euler.unsorted

import com.ewyx.euler.core.{ EulerProblem, Utils, PolynomialUtils, PolynomialElement }

/**
 * It is possible to write ten as the sum of primes in exactly five different ways:
 *
 * 7 + 3
 * 5 + 5
 * 5 + 3 + 2
 * 3 + 3 + 2 + 2
 * 2 + 2 + 2 + 2 + 2
 *
 * What is the first value which can be written as the sum of primes in over five thousand different ways?
 *
 * Just use the generator function that was used for problem 76 and apply it to only use prime numbers, instead of
 * every number
 */
class Euler77 extends EulerProblem[Int] {
  def solve() = {
    def stepPolynomial(step: Int, max: Int) = {
      for {
        x <- 0 to 100
        y = x * step
        if (y <= max)
      } yield PolynomialElement(1, y)
    }

    val polynomials = for {
      i <- Utils.primes().takeWhile(_ <= 1000)

    } yield stepPolynomial(i, 1000)

    polynomials
      .foldLeft(Seq(PolynomialElement(1, 0)))(PolynomialUtils.multiply(1000))
      .sortBy(_.power)
      .dropWhile(_.a < 5000)
      .head
      .power
  }
}
