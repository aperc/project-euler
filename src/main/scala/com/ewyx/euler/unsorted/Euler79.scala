package com.ewyx.euler.unsorted
import com.ewyx.euler.core.Utils._

/**
 *
 * A common security method used for online banking is to ask the user for
 * three random characters from a passcode. For example, if the passcode was 531278,
 * they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.
 *
 * The text file, keylog.txt, contains fifty successful login attempts.
 *
 * Given that the three characters are always asked for in order,
 * analyse the file so as to determine the shortest possible secret passcode of unknown length.
 *
 *
 */
object Euler79 {
  def solve() = {
    val keylog = List(319, 680, 180, 690, 129, 620, 762, 689, 762, 318,
      368, 710, 720, 710, 629, 168, 160, 689, 716, 731, 736, 729, 316, 729,
      729, 710, 769, 290, 719, 680, 318, 389, 162, 289, 162, 718, 729, 319,
      790, 680, 890, 362, 319, 760, 316, 729, 380, 319, 728, 716)

    //get all distinct numbers (assuming not every number between 1 and 9 is present.
    val numbers = (keylog.map(_.toString) reduce (_ + _) distinct).map(_.asDigit)

    //check if a number matches our whole keylog
    //as soon as it doesn't return with false.
    def matchesLog(n: Seq[Int]): Boolean = {
      for {
        num <- keylog
        if (n.filter(num.digitList.contains(_)).toList != num.digitList)
      } return false
      true
    }

    //get all permutations, and see which one is the first that fits
    (for {
      n <- numbers.permutations
    } yield n).toList
    numbers.permutations.filter(matchesLog(_)).toList.head
  }
}