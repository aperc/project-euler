package com.ewyx.euler.unsorted
import com.ewyx.euler._
import com.ewyx.euler.core._
import com.ewyx.euler.core.Graphs.Vertex

object Euler82 {
  def solve() = {
    val file = loadFile("p081_matrix.txt").getLines
    val graph = file.map(_.split(',').map(_.toInt).map(new Vertex(_)).toVector).toVector

    for {
      x <- 0 to 79;
      y <- 0 to 79
    } {
      if (x + 1 < 80) graph(x)(y).connect(graph(x + 1)(y))
      if (y + 1 < 80) graph(x)(y).connect(graph(x)(y + 1))
      if (x - 1 > 0) graph(x)(y).connect(graph(x - 1)(y))
    }
    val start = new Vertex(0) //create start vertex and connect it to all the first rows
    for {
      x <- 0 to 79
    } start.connect(graph(x)(0))
    val end = new Vertex(0) //connect all the end vertexes to the end row
    for {
      x <- 0 to 79
    } graph(x)(79).connect(end)
    Graphs.dijkstra(start :: end :: graph.flatten.toList, start).get(end)
  }
}