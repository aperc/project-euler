package com.ewyx.euler.unsorted

import com.ewyx.euler._
import com.ewyx.euler.core._
import com.ewyx.euler.core.Graphs.Vertex

object Euler83 {

  def solve() = {
    val file = loadFile("p081_matrix.txt").getLines
    val graph = file.map(_.split(',').map(_.toInt).map(new Vertex(_)).toVector).toVector

    for {
      x <- 0 to 79;
      y <- 0 to 79
    } {
      if (x + 1 < 80) graph(x)(y).connect(graph(x + 1)(y))
      if (y + 1 < 80) graph(x)(y).connect(graph(x)(y + 1))
      if (x - 1 > 0) graph(x)(y).connect(graph(x - 1)(y))
      if (y - 1 > 0) graph(x)(y).connect(graph(x)(y - 1))
    }

    Graphs.dijkstra(graph.flatten.toList, graph(0)(0)).get(graph(79)(79))
  }
}