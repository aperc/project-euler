package com.ewyx.euler.unsorted

import com.ewyx.euler.core.EulerProblem

import scala.util.Random

// Alternative solution is to model with the help of Markov and multiply the matrix by itself, that the solution is
// faster, due to markov chains converging more quickly towards its finaly state

class Euler84 extends EulerProblem[Int] {

  private case class GameState(position: Int, board: Vector[Int])
  //keep track of number of times visited
  private val startingBoard: Vector[Int] = Stream.continually(0).take(40).toVector
  private val startingPosition = 0
  private val startState = GameState(startingPosition, startingBoard)

  private lazy val d1 = Stream.continually(Random.nextInt(4) + 1)
  private lazy val d2 = Stream.continually(Random.nextInt(4) + 1)

  private def visit(position: Int)(state: GameState): GameState = {
    GameState(position, state.board.updated(position, state.board(position) + 1))
  }

  private def goToJail: GameState => GameState = visit(10)

  private def resolveChest(position: Int): Int = {
    val change = Random.nextInt(16)
    change match {
      case 0 => 10
      case 1 => 0
      case _ => position
    }
  }

  private def resolveChance(position: Int): Int = {
    val change = Random.nextInt(16)
    change match {
      case 0 => 0
      case 1 => 10
      case 2 => 11
      case 3 => 24
      case 4 => 39
      case 5 => 5
      case 6 | 7 => ((position / 10) * 10) + 5
      case 8 => {
        if (position > 12 && position < 28) {
          28
        } else {
          12
        }
      }
      case 9 => position - 3
      case _ => position
    }
  }

  private def move(gameState: GameState, rolls: Stream[(Int, Int)]): GameState = {
    if (rolls.forall { case (d1, d2) => d1 - d2 == 0 }) {
      goToJail(gameState)
    } else {
      val newPos = (gameState.position + rolls.last._1 + rolls.last._2) % 40
      (if (List(2, 17, 33).contains(newPos)) { // community chest positions
        visit(resolveChest(newPos)) _
      } else if (List(7, 22, 36).contains(newPos)) { // chance positions
        visit(resolveChance(newPos)) _
      } else if (newPos == 30) { // go to jail position
        goToJail
      } else { // else move normally
        visit(newPos) _
      })(gameState)
    }
  }
  private val throws = 100000

  def solve() = d1.zip(d2) //infinite stream of random dice throws
    .take(throws) //take 100k throws
    .sliding(3) //keep track of three throws (for go to jail purposes)
    .foldLeft(startState)(move) //update state for each throw
    .board // get board
    .map(x => x / throws.toDouble) //divide with number of throws to get probabilities
    .zipWithIndex //zip to get board positions
    .sortBy(-_._1) //sort by probability
    .take(3) //take the top 3
    .map(_._2).mkString.toInt

}
