package com.ewyx.euler.unsorted

import com.ewyx.euler.core.EulerProblem

/**
 * By counting carefully it can be seen that a rectangular grid measuring 3 by 2 contains eighteen rectangles:
 * [image available at https://projecteuler.net/problem=85]
 * Although there exists no rectangular grid that contains exactly two million rectangles, find the area of the grid
 * with the nearest solution.
 */
class Euler85 extends EulerProblem[Int] {
  def solve() = {
    def squares(width: Int, height: Int) = {
      for {
        x <- 1 to width
        y <- 1 to height
      } yield (1 + width - x) * (1 + height - y)
    }

    val rectangle = (for {
      x <- 1 to 100
      y <- 1 to 100
    } yield (x, y, squares(x, y).sum))
      .filter(_._3 < 2000000)
      .sortBy(_._3)
      .last

    rectangle._1 * rectangle._2
  }
}
