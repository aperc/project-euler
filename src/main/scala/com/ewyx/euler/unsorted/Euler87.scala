package euler
import scala.collection.mutable.HashSet
import com.ewyx.euler.core.Utils

object Euler87 {
  def solve() = {
    val p = Utils.sieveOfEratosthenes(10000)
    val LIMIT = 50000000
    val s = HashSet[Int]()
    val psq = p.map(x => x * x).takeWhile(_ <= LIMIT)
    val pcb = p.map(x => x * x * x).takeWhile(_ <= LIMIT)
    val pqd = p.map(x => x * x * x * x).takeWhile(_ <= LIMIT)
    val l = for {
      i <- psq
      j <- pcb.takeWhile(_ + i <= LIMIT)
      k <- pqd.takeWhile(_ + i + j <= LIMIT)
    } s += (i + j + k)
    s size
  }
}
