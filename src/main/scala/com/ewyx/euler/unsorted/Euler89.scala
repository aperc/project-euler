package com.ewyx.euler.unsorted

import com.ewyx.euler.core.RomanNumeralParser
import com.ewyx.euler._

object Euler89 {

  def compareLength(romanNumeral: String): Int = {
    val num = RomanNumeralParser(romanNumeral).get.sum
    romanNumeral.length - RomanNumeralParser.toRomanNumeral(num).length
  }

  def solve() = {
    val file = loadFile("p089_roman.txt").getLines
    val savings = for {
      line <- file
    } yield compareLength(line.trim)
    savings sum
  }
}