package com.ewyx.euler.unsorted

import com.ewyx.euler.core.EulerProblem

class Euler90 extends EulerProblem[Int] {
  val dice = (0 to 9)
    .permutations
    .map(_.take(6).map(_.toString).toSet)
    .toSet

  def extendSet(s: Set[String]): Set[String] = {
    if (s.contains("6") || s.contains("9")) {
      s + "9" + "6"
    } else {
      s
    }
  }
  def canRepresentSquares(d1: Set[String], d2: Set[String]): Boolean = {
    val cubes = Set("01", "04", "09", "16", "25", "36", "49", "64", "81")

    def check(d1: Set[String], d2: Set[String], cube: String): Boolean = {
      d1.contains(cube.substring(0, 1)) &&
        d2.contains(cube.substring(1))
    }

    cubes.forall(str => check(d1, d2, str) || check(d2, d1, str))
  }

  override def solve(): Int = {
    val arrangements = for {
      d1 <- dice
      d2 <- dice.dropWhile(_ != d1)
      extendedD1 = extendSet(d1)
      extendedD2 = extendSet(d2)
      if canRepresentSquares(extendedD1, extendedD2)
    } yield (d1, d2)

    arrangements.size
  }
}
