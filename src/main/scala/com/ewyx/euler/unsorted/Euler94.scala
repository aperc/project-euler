package com.ewyx.euler.unsorted
import com.ewyx.euler.core.{ Utils, EulerProblem }
import com.ewyx.euler.core.Utils._
import scala.collection.mutable.HashMap

class Euler94 extends EulerProblem[Int] {

  def euclidsFormula(k: Int, m: Int, n: Int): (Int, Int, Int) = {
    (k * (m * m - n * n), k * (2 * m * n), k * (m * m + n * n))
  }

  def tupleSum(x: (Int, Int, Int)): Int = x._1 + x._2 + x._3

  val triples = for {
    m <- 1 to 12910
    n <- (1 to 12910).takeWhile(x => x < m)
    if (Utils.coprimes(m, n) && (m - n) % 2 == 1)
    triangle = euclidsFormula(1, m, n)
    if (math.abs(2 * triangle._1 - triangle._3) == 1 || math.abs(2 * triangle._2 - triangle._3) == 1)
  } yield triangle

  def solve() = {
    triples
      .distinct
      .filter(_._3 < 333333333)
      .map {
        case (a, b, c) =>
          if (a < b) {
            2 * a + 2 * c
          } else {
            2 * b + 2 * c
          }
      }.sum
  }
}