package com.ewyx.euler.unsorted

import com.ewyx.euler._
import com.ewyx.euler.core.Utils

object Euler96 {
  type Puzzle = Array[Array[Int]]

  /*returns the numbers of the corresponding square in a list*/
  def square(x: Int, y: Int, p: Puzzle): List[Int] = {
    var cx = x / 3
    var cy = y / 3
    (for {
      i <- 0 to 2
      j <- 0 to 2
    } yield p(i + cx * 3)(j + cy * 3)) toList
  }

  //Finds all candidates for the spot
  def findCandidates(x: Int, y: Int, puzzle: Puzzle): List[Int] = {
    (for {
      candidate <- 1 to 9
      if (!puzzle(x).contains(candidate) &&
        !puzzle.map(_(y)).contains(candidate) &&
        !square(x, y, puzzle).contains(candidate))
    } yield candidate) toList
  }
  //fill the obvious choices
  def fillSimple(puzzle: Puzzle): Puzzle = {
    var p = puzzle
    for {
      x <- 0 to 8
      y <- 0 to 8
      if (p(x)(y) == 0 && findCandidates(x, y, p).length == 1)
    } p.updated(x, p(x).updated(y, findCandidates(x, y, p).head))
    p
  }

  def firstZero(puzzle: Puzzle): (Int, Int) = {
    for {
      x <- 0 to 8
      y <- 0 to 8
      if (puzzle(x)(y) == 0)
    } return (x, y)
    (-1, -1)
  }

  def findSolution(puzzle: Puzzle): List[Puzzle] = {
    def updatePuzzle(x: Int, y: Int, el: Int): Puzzle = {
      puzzle.updated(x, puzzle(x).updated(y, el))
    }
    val fZ = firstZero(puzzle)
    if (fZ._1 != -1) {
      val candidates = findCandidates(fZ._1, fZ._2, puzzle)
      (for {
        c <- candidates
      } yield findSolution(updatePuzzle(fZ._1, fZ._2, c))) flatten
    } else {
      List(puzzle)
    }
  }

  def solvePuzzle(p: Puzzle): Option[Puzzle] = findSolution(p).headOption

  def getPuzzles(): List[Puzzle] = {
    val puzzleLines = loadFile("sudoku.txt").getLines.sliding(10, 10)
    (for {
      lines <- puzzleLines
    } yield lines.drop(1).map(x => x.map(_.asDigit).toArray).toArray).toList
  }

  def solve() = {
    val puzzles = getPuzzles() map fillSimple
    puzzles
      .map(findSolution)
      .map(_.head.head.take(3).toList)
      .map(Utils.digitsToInt(_))
      .sum
  }
}