package euler
/**
 *
 * The first known prime found to exceed one million digits was discovered in 1999, and is a Mersenne prime of the form 26972593 - 1; it contains exactly 2,098,960 digits. Subsequently other Mersenne primes, of the form 2p - 1, have been found which contain more digits.
 *
 * However, in 2004 there was found a massive non-Mersenne prime which contains 2,357,207 digits: 28433^27830457+1.
 *
 * Find the last ten digits of this prime number.
 *
 * Gist: since we're looking at the last 10 digits, we can keep discarding anything above that, hence the mod%10000000000.
 * Powers of two are basically left shifts, so we do that a couple million times, it's pretty fast on modern day computers anyway.
 * In the end we insert the last 10 digits into the equation, again, anything above that can easily be discarded.
 */
object Euler97 {
  def solve() = {
    val moduloValue = 10000000000L
    val counter = 7830457
    val start = 2L
    val res = (1 until counter).foldLeft(start)((acc, el) => (acc << 1) % moduloValue)
    ((28433L * res) + 1) % moduloValue
  }
}