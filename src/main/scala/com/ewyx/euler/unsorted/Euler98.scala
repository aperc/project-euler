package com.ewyx.euler.unsorted

import com.ewyx.euler._

class Euler98 {
  val squares = Stream.from(1)
    .map(x => x * x)
    .filter(x => x.toString.toSet[Char].size == x.toString.length)
    .takeWhile(_ < 1000000000).toSet

  def isMatch(w1: String, w2: String, n1: Int, n2: Int): Boolean = {
    w1.map(x => (w1.indexOf(x), w2.indexOf(x)))
      .forall(x => n1.toString.charAt(x._1) == n2.toString.charAt(x._2))
  }

  def anagrams(): List[(String, String)] = {
    val file = loadFile("euler42.txt")
    //make a list, remove quotes
    val words = file.getLines().mkString.split(',').toList.map(_.drop(1).dropRight(1))
    for {
      w1 <- words
      w2 <- words.dropWhile(_ != w1)
      if w1.length == w2.length && w1 != w2 && w1.sorted == w2.sorted
    } yield (w1, w2)
  }

  def findMatch(w1: String, w2: String) = {
    val opts = squares.filter(_.toString.length == w1.length)
    for {
      x <- opts
      y <- opts
      if isMatch(w1, w2, x, y)
    } yield (x, y)
  }

  def solve() = {
    anagrams
      .sortBy(_._1.length)
      .reverse
      .toStream
      .map(x => findMatch(x._1, x._2))
      .dropWhile(_.isEmpty)
      .head
  }
}