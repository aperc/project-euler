package euler

import scala.annotation.tailrec

object Euler303 {
  @tailrec
  def bigDigits(n: Int, d: List[Int] = List()): List[Int] = {
    n match {
      case n: Int if (n / 10 == 0) => n :: d
      case _ => bigDigits(n / 10, (n % 10 :: d))
    }
  }

  def smallDigits(n: Int): Boolean = {
    val digits = bigDigits(n)
    digits.filter(x => x <= 2).length == digits.length
  }

  def leastPosMul(n: Int) = {
    var mul: Int = n
    var cnt = 1
    while (!smallDigits(mul)) {
      mul = mul + n
      cnt = cnt + 1
    }
    println(mul)
    cnt
  }

  def solve() = {
    //		val muls = for {
    //			x <- 9500 to 10000
    //		} yield leastPosMul(x)     

    leastPosMul(99)
  }
}