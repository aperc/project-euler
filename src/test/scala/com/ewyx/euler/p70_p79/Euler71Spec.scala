package com.ewyx.euler.p70_p79

import com.ewyx.euler.unsorted.Euler71
import org.scalatest.{ Matchers, PropSpec }

class Euler71Spec extends PropSpec with Matchers {

  val solver = new Euler71()

  property("The solution to problem 71 should be 428570") {
    solver.solve() should be(BigInt(428570))
  }

}
