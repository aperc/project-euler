package com.ewyx.euler.p70_p79

import com.ewyx.euler.unsorted.{ Euler76, Euler71 }
import org.scalatest.{ Matchers, PropSpec }

class Euler76Spec extends PropSpec with Matchers {

  val solver = new Euler76()

  property("The solution to problem 76 should be 190569291") {
    solver.solve() should be(190569291)
  }

}
