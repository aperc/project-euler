package com.ewyx.euler.p70_p79

import com.ewyx.euler.unsorted.{ Euler77, Euler76, Euler71 }
import org.scalatest.{ Matchers, PropSpec }

class Euler77Spec extends PropSpec with Matchers {

  val solver = new Euler77()

  property("The solution to problem 76 should be 71") {
    solver.solve() should be(71)
  }

}
