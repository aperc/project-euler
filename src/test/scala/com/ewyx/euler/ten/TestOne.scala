package com.ewyx.euler.ten

import com.ewyx.euler._0x.Euler1
import org.scalatest.{ Matchers, FlatSpec }

class TestOne extends FlatSpec with Matchers {

  "Euler1" should "return the solution" in {
    (new Euler1).solve() should be(233168)
  }
}
