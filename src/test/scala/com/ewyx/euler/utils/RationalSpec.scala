package com.ewyx.euler.utils

import com.ewyx.euler.core.Rational
import com.ewyx.euler.core.Rational.RationalIsNumeric._
import org.scalatest.{ FlatSpec, Matchers }

class RationalSpec extends FlatSpec with Matchers {
  behavior of "Rational"

  it should "correctly compare numbers" in {
    val a = Rational(2, 7)
    val b = Rational(1, 2)
    val c = Rational(1, 3)
    val d = Rational(1, 1)
    (b > a) should be(true)
    (b > c) should be(true)
    (d < c) should be(false)
    List(a, b, c, d).sorted should be(List(a, c, b, d))

  }
}
